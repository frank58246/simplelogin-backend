using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using OAuth.Repository;
using SimpleLogin.Backend.Repository.Interfaces;
using SimpleLogin.Backend.Services;
using SimpleLogin.Backend.Services.Interfaces;

namespace OAuth
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllersWithViews();
            services.AddTransient<ILineNotifyService,LineNotifyService>();
            services.AddTransient<ILineNotifyService, LineNotifyService>();
            services.AddTransient<ILineNotifyApiRepository, LineNotifyApiRepository>();
            services.AddDistributedMemoryCache();
            services.AddSingleton<ILineNotifyTokenRepository, LineNotifyTokenRepository>();
            services.AddSession(options =>
            {                
                options.IdleTimeout = TimeSpan.FromDays(1);
                
                options.Cookie.HttpOnly = false;
                
                options.Cookie.IsEssential = true;
                options.Cookie.Name = "MySessionCookie";

            });

            services.AddHttpClient();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Home/Error");
            }
            app.UseStaticFiles();

            app.UseRouting();
            app.UseSession();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllerRoute(
                    name: "default",
                    pattern: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
