﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using OAuth.Repository;

namespace SimpleLogin.Backend.Repository.Interfaces
{
    public interface ILineNotifyApiRepository
    {
        public Task<string> RevokeAsync(string token);

        public Task<bool> SendMessageAsync(string message, string token);
        public Task<BaseLineResponse> GetTokenStatusAsync(string message);
    }
}
