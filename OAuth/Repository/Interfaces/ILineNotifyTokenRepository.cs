﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SimpleLogin.Backend.Repository.Interfaces
{
    public interface ILineNotifyTokenRepository
    {
        public bool Save(string token);

        public bool Remove(string token);
        public IEnumerable<string> GetAll();

        
    }
}
