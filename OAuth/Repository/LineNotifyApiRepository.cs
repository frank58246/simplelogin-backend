﻿using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using SimpleLogin.Backend.Extensions;
using SimpleLogin.Backend.Repository.Interfaces;

namespace OAuth.Repository
{
    public class LineNotifyApiRepository : ILineNotifyApiRepository
    {
        private readonly HttpClient _httpClient;

        public LineNotifyApiRepository(IHttpClientFactory httpClientFactory)
        {
            _httpClient = httpClientFactory.CreateClient() ;
        }

        public async Task<bool> SendMessageAsync(string message, string token)
        {
            var url = "https://notify-api.line.me/api/notify";
            var payLoad = new Dictionary<string, string>
            {
                { "message", message}
            };
            var response = await this._httpClient.PostFormAsync<BaseLineResponse>(url, payLoad, token);
            return true;
        }
        

        public async Task<BaseLineResponse> GetTokenStatusAsync(string token)
        {
            var url = "https://notify-api.line.me/api/status";           
            return await this._httpClient.GetAsync<BaseLineResponse>(url, token);
        }

        public async Task<string> RevokeAsync(string token)
        {
            var url = "https://notify-api.line.me/api/revoke";

            var payload = new Dictionary<string, string>();

            var response  = await this._httpClient.PostFormAsync<BaseLineResponse>(url,payload,token);

            return response.Message;
        }

        
    }
}
