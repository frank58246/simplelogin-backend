﻿using Newtonsoft.Json;

namespace OAuth.Repository
{
    public class BaseLineResponse
    {
        [JsonProperty("status")]
        public int Status { get; set; }

        [JsonProperty("message")]
        public string Message { get; set; }
    }
}
