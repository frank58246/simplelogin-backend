﻿using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using OAuth.Models;
using SimpleLogin.Backend.Extensions;
using SimpleLogin.Backend.Models.Request;
using SimpleLogin.Backend.Services.Interfaces;

namespace OAuth.Controllers
{
    public class LineNotifyController : Controller
    {
        private readonly HttpClient _httpClient;
        private readonly ILineNotifyService _lineNotifyService;

        private const string SESSION_NAME = "linenotify";

        private const string STATE = "6666";

        public LineNotifyController(IHttpClientFactory httpClientFactory, ILineNotifyService lineNotifyService)
        {
            _httpClient = httpClientFactory.CreateClient();
            _lineNotifyService = lineNotifyService;
        }

      
        public async Task<IActionResult> Callback(string state, string code)
        {
            if (state == STATE)
            {
                // get Token
                string url = "https://notify-bot.line.me/oauth/token";
                var payLoad = new Dictionary<string, string>()
                    {
                        { "grant_type", "authorization_code"},
                        { "code", code},
                        { "redirect_uri", "https://simplelogin64285-backend.herokuapp.com/linenotify/callback" },
                        { "client_id", "pZCvRTM2GzHGTBTgiWdQNf"},
                        { "client_secret", "JCxj2G4CVeyH24cD3FvmxcR81PWldvLco1yIJ5izCls"}
                    };

                var tokenResponse = await _httpClient.PostFormAsync<LineNotifyTokenResponse>(url, payLoad);

                // save token
                this._lineNotifyService.AddInTokenMemory(tokenResponse.AccessToken);
                this.SaveTokenInSession(tokenResponse.AccessToken);

                return Redirect("/Home");
                //return View("Callback", tokenResponse.Message + $" your token is {tokenResponse.AccessToken}");

            }
            
            return Redirect("/Home");

        }

        public async Task<IActionResult> Revoke()
        {
            if (!HasTokenInSession())
            {
                return Redirect("/lineNotify");
            }

            // clear session, memory, then revoke
            var token = this.GetTokenInSession();

            this.ClearTokenInSession();

            this._lineNotifyService.RemoveTokenInMemory(token);

            await this._lineNotifyService.RevokeTokenAsync(token);

            return Redirect("/lineNotify");
        }



        private bool HasTokenInSession()
        {
            var value = HttpContext.Session.GetString(SESSION_NAME);

            return !string.IsNullOrEmpty(value);
        }

        private string GetTokenInSession()
        {
            return HttpContext.Session.GetString(SESSION_NAME);
        }

        private void SaveTokenInSession(string token)
        {
            HttpContext.Session.SetString(SESSION_NAME, token);
        }


        private void ClearTokenInSession()
        {
            HttpContext.Session.SetString(SESSION_NAME, string.Empty);
        }
    }
}
