﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using OAuth.Models;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Newtonsoft.Json;
using OAuth.Models.Response;
using SimpleLogin.Backend.Models.Request;
using SimpleLogin.Backend.Services.Interfaces;

namespace OAuth.Controllers
{
    public class HomeController : Controller
    {
        private readonly ILogger<HomeController> _logger;
        private readonly ILineNotifyService _lineNotifyService;
        private const string SESSION_NAME = "line";
        private const string NOTIFY_SESSION_NAME = "linenotify";
        
        public HomeController(ILogger<HomeController> logger, ILineNotifyService lineNotifyService)
        {
            _logger = logger;
            _lineNotifyService = lineNotifyService;
        }

        public IActionResult Index()
        {
            var profileFromSession = GetProfileFromSession();
            var profile = new ProfileViewModel()
            {
                Name = profileFromSession?.DisplayName,
                Image = profileFromSession?.PictureUrl,
                NotifyAuthUrl = GetNotifyRequestUrl(),
                NotifyToken = GetTokenInSession(),
                DebugInfo = string.Join(",", HttpContext.Session.Keys)
            };

            if (profileFromSession == null)
            {
                return Redirect("/Login");
            }
            
            return View("Index",profile);
        }


        [HttpPost]
        public async Task<bool> SendMessage(string message)
        {
            return  await this._lineNotifyService.SendMessageToAllAsync(message);
        }

        public  async Task<IActionResult> Revoke()
        {
            var token = this.GetTokenInSession();

            this.ClearTokenInSession();

            this._lineNotifyService.RemoveTokenInMemory(token);

            await this._lineNotifyService.RevokeTokenAsync(token);

            return Redirect("/Home");
        }

        private string GetNotifyRequestUrl()
        {
            // show auth url
            var request = new AuthorizeRequest
            {
                AuthUrl = "https://notify-bot.line.me/oauth/authorize",
                ResponseType = "code",
                ClientId = "pZCvRTM2GzHGTBTgiWdQNf",
                RedirectUri = "https://simplelogin64285-backend.herokuapp.com/linenotify/callback",
                State = "6666",
                Scope = "notify",
            };

            return request.ToRequestUrl();
        }

        private LineProfileResponse GetProfileFromSession()
        {
            var value = HttpContext.Session.GetString(SESSION_NAME);
            try
            {
                return JsonConvert.DeserializeObject<LineProfileResponse>(value);          

            }
            catch (Exception e)
            {
                return null;
            }
        }
        
        private string GetTokenInSession()
        {
            return HttpContext.Session.GetString(NOTIFY_SESSION_NAME);
        }
        
        private void ClearTokenInSession()
        {
            HttpContext.Session.SetString(NOTIFY_SESSION_NAME, string.Empty);
        }
    }
}
