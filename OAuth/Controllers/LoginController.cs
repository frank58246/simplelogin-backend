using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using OAuth.Models;
using OAuth.Models.Response;
using SimpleLogin.Backend.Extensions;
using SimpleLogin.Backend.Models.Request;

namespace OAuth.Controllers
{
    public class LoginController: Controller
    {
        private const string SESSION_NAME = "line";
        private const string CLIENT_ID = "1657007023";
        private const string CLIENT_SECRET = "0738e9104c35a61e3962639b5379d25e";
        private const string STATE = "6666";
        private const string REDIRECT_URL = "https://simplelogin64285-backend.herokuapp.com/login/callback";
        
        private readonly HttpClient _httpClient;

        public LoginController(IHttpClientFactory httpClientFactory)
        {
            _httpClient = httpClientFactory.CreateClient();
        }

        public IActionResult Index()
        {
            
            // show auth url
            var request = new AuthorizeRequest
            {
                AuthUrl = "https://access.line.me/oauth2/v2.1/authorize",
                ResponseType = "code",
                ClientId = "1657007023",
                RedirectUri =  REDIRECT_URL,
                State = STATE,
                Scope = "openid profile"
            };

            var profileInSession = GetProfileFromSession();
            if (profileInSession != null)
            {
                return Redirect("/Home");
            }
            
            return View("Index", request.ToRequestUrl());
        }

        public async Task<IActionResult> Callback(string state, string code)
        {
            // get Token
            string url = "https://api.line.me/oauth2/v2.1/token";
            var payLoad = new Dictionary<string, string>()
            {
                { "grant_type", "authorization_code"},
                { "code", code},
                { "redirect_uri", REDIRECT_URL },
                { "client_id", CLIENT_ID},
                { "client_secret", CLIENT_SECRET}
            };

            var tokenReseponse = await _httpClient.PostFormAsync<LineLoginTokenResponse>(url, payLoad);
            if (!string.IsNullOrEmpty(tokenReseponse.Error))
            {
                return Redirect("/linelogin");
            }
         

            // get User Profile
            var profileUrl = "https://api.line.me/v2/profile";
            var profile = await _httpClient.GetAsync<LineProfileResponse>(profileUrl, tokenReseponse.AccessToken);
            SaveProfileInSession(profile);
            
            return Redirect("/Home");

        }
        
        private void SaveProfileInSession(LineProfileResponse profile)
        {
            var jsonString = JsonConvert.SerializeObject(profile);
            HttpContext.Session.SetString(SESSION_NAME, jsonString);
        }
        
        private LineProfileResponse GetProfileFromSession()
        {
            var value = HttpContext.Session.GetString(SESSION_NAME);
            try
            {
                return JsonConvert.DeserializeObject<LineProfileResponse>(value);          

            }
            catch (Exception e)
            {
                return null;
            }
        }
      
    }
}