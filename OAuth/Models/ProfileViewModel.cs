namespace OAuth.Models
{
    public class ProfileViewModel
    {
        public string Name { get; set; }

        public string Image { get; set; }

        public string NotifyToken { get; set; }

        public string NotifyAuthUrl { get; set; }
        public string DebugInfo { get; set; }
    }
}