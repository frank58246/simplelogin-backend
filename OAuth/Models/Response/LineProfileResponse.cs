﻿using Newtonsoft.Json;

namespace OAuth.Models.Response
{
    public class LineProfileResponse
    {
   

        [JsonProperty("displayName")]
        public string DisplayName { get; set; }

        [JsonProperty("pictureUrl")]
        public string PictureUrl { get; set; }

    }
}
