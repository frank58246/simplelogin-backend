using Newtonsoft.Json;

namespace OAuth.Models.Response
{
    public class LineLoginTokenResponse
    {
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }

        [JsonProperty("error")]
        public string Error { get; set; }
    }
}