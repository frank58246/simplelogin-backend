﻿using Newtonsoft.Json;
using OAuth.Repository;

namespace OAuth.Models
{
    public class LineNotifyTokenResponse: BaseLineResponse
    {
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
    }
}
