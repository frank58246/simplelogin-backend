﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SimpleLogin.Backend.Models.Request
{
    public class AuthorizeRequest
    {

        [JsonProperty("authUrl")]
        public string AuthUrl { get; set; }

        [JsonProperty("response_type")]
        public string ResponseType { get; set; }

        [JsonProperty("client_id")]
        public string ClientId { get; set; }

        [JsonProperty("redirect_uri")]
        public string RedirectUri { get; set; }

        [JsonProperty("state")]
        public string State { get; set; }
        
        [JsonProperty("scope")]
        public string Scope { get; set; }

        [JsonProperty("response_mode")]
        public string ResponseMode { get; set; }

        public string ToRequestUrl()
        {
            return string.Format("{0}?response_type={1}" +
                "&client_id={2}" +
                "&redirect_uri={3}" +
                "&state={4}" +
                "&scope={5}" +
                "&response_mode={6}", AuthUrl, ResponseType, ClientId, RedirectUri, State, Scope, ResponseMode);               

        }
    }
}
