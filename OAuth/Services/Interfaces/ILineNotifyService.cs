﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SimpleLogin.Backend.Services.Interfaces
{
    public interface ILineNotifyService
    {
        public Task<string> RevokeAllAsync();
        public Task<string> RevokeTokenAsync(string token);
        public Task<bool> SendMessageToAllAsync(string message); 
        public Task<bool> IsValidateAsync(string token);

        public void RemoveTokenInMemory(string token);
        public void AddInTokenMemory(string token);
    }
}
