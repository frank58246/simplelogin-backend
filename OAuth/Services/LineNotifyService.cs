﻿using SimpleLogin.Backend.Repository.Interfaces;
using SimpleLogin.Backend.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace SimpleLogin.Backend.Services
{
    public class LineNotifyService : ILineNotifyService
    {
        
        private readonly ILineNotifyTokenRepository _lineNotifyTokenRepository;
        private readonly ILineNotifyApiRepository _lineNotifyApiRepository;

        public LineNotifyService(ILineNotifyApiRepository lineNotifyApiRepository, ILineNotifyTokenRepository lineNotifyTokenRepository)
        {
            _lineNotifyApiRepository = lineNotifyApiRepository;
            _lineNotifyTokenRepository = lineNotifyTokenRepository;
        }


        public async Task<bool> SendMessageToAllAsync(string message)
        {
            var url = "https://notify-api.line.me/api/notify";
            var tokens = this._lineNotifyTokenRepository.GetAll();
            var result = new StringBuilder();
            foreach (var token in tokens)
            {
                var payLoad = new Dictionary<string, string>
                {
                    { "message", message}
                };

               await this._lineNotifyApiRepository.SendMessageAsync(message, token);
            }

            return true;
        }

        public async Task<bool> IsValidateAsync(string token)
        {
            var response = await this._lineNotifyApiRepository.GetTokenStatusAsync(token);
            return response.Status == (int)HttpStatusCode.OK;
        }

     

        public async Task<string> RevokeAllAsync()
        {
            var savedToken = this._lineNotifyTokenRepository.GetAll();
            if (!savedToken.Any())
            {
                return "no saved token ";
            }
            var result = new StringBuilder();
            foreach (var token in savedToken)
            {
                var response = await this.RevokeTokenAsync(token);

                result.AppendLine(response);
            }

            return result.ToString();
        }

        public async Task<string> RevokeTokenAsync(string token)
        {
            this._lineNotifyTokenRepository.Remove(token);

            return await this._lineNotifyApiRepository.RevokeAsync(token);
        }

        public void RemoveTokenInMemory(string token)
        {
            this._lineNotifyTokenRepository.Remove(token);

        }
        public void AddInTokenMemory(string token)
        {
            this._lineNotifyTokenRepository.Save(token);
        }
    }
}
