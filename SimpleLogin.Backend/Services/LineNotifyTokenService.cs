﻿using SimpleLogin.Backend.Repository.Interfaces;
using SimpleLogin.Backend.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace SimpleLogin.Backend.Services
{
    public class LineNotifyTokenService : ILineNotifyTokenService
    {
        
        private readonly ILineNotifyTokenRepository _lineNotifyTokenRepository;
        private readonly ILineNotifyApiRepository _lineNotifyApiRepository;

        public LineNotifyTokenService(ILineNotifyApiRepository lineNotifyApiRepository, ILineNotifyTokenRepository lineNotifyTokenRepository)
        {
            _lineNotifyApiRepository = lineNotifyApiRepository;
            _lineNotifyTokenRepository = lineNotifyTokenRepository;
        }

        

        public async Task<bool> IsValidateAsync(string token)
        {
            var response = await this._lineNotifyApiRepository.GetTokenStatusAsync(token);
            return response.Status == (int)HttpStatusCode.OK;
        }

     

        public async Task<string> RevokeAllAsync()
        {
            var savedToken = this._lineNotifyTokenRepository.GetAll();
            if (!savedToken.Any())
            {
                return "no saved token ";
            }
            var result = new StringBuilder();
            foreach (var token in savedToken)
            {
                var response = await this.RevokeAsync(token);

                result.AppendLine(response);
            }

            return result.ToString();
        }

        public async Task<string> RevokeAsync(string token)
        {
            this._lineNotifyTokenRepository.Remove(token);

            return await this._lineNotifyApiRepository.RevokeAsync(token);
        }

        public void RemoveInMemory(string token)
        {
            this._lineNotifyTokenRepository.Remove(token);

        }
        public void AddInMemory(string token)
        {
            this._lineNotifyTokenRepository.Save(token);
        }
    }
}
