﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SimpleLogin.Backend.Services.Interfaces
{
    public interface ILineNotifyTokenService
    {
        public Task<string> RevokeAllAsync();
        public Task<string> RevokeAsync(string token);

        public Task<bool> IsValidateAsync(string token);

        public void RemoveInMemory(string token);
        public void AddInMemory(string token);
    }
}
