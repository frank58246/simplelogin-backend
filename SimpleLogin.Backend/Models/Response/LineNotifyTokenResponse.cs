﻿using Newtonsoft.Json;
using SimpleLogin.Backend.Models.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SimpleLogin.Backend.Models
{
    public class LineNotifyTokenResponse: BaseLineResponse
    {
        [JsonProperty("access_token")]
        public string AccessToken { get; set; }
    }
}
