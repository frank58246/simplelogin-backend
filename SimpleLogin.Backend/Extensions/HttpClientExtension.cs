﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace SimpleLogin.Backend.Extensions
{
    public static class HttpClientExtension
    {
        public static async Task<T> PostFormAsync<T>(this HttpClient httpClient,
            string url,
            Dictionary<string, string> payload,
            string bearerToken = null)
        {

            var pair = new List<KeyValuePair<string, string>>();

            foreach (var item in payload)
            {
                pair.Add(new KeyValuePair<string, string>(item.Key, item.Value));
            }
          
            var requestMessage = new HttpRequestMessage(HttpMethod.Post, url)
            {
                Content = new FormUrlEncodedContent(pair) 
            };

            if (!string.IsNullOrEmpty(bearerToken))
            {
                requestMessage.Headers.Add("Authorization", $"Bearer {bearerToken}");
            }

            var response = await httpClient.SendAsync(requestMessage);

            var message = await response.Content.ReadAsStringAsync();

            return JsonConvert.DeserializeObject<T>(message);
        }

        public static async Task<T> GetAsync<T>(this HttpClient httpClient, 
            string url,
            string bearerToken = null)
        {
            var requestMessage = new HttpRequestMessage(HttpMethod.Get, url);
            if (!string.IsNullOrEmpty(bearerToken))
            {
                requestMessage.Headers.Add("Authorization", $"Bearer {bearerToken}");
            }

            var response = await httpClient.SendAsync(requestMessage);

            var message = await response.Content.ReadAsStringAsync();

            return JsonConvert.DeserializeObject<T>(message);
        }
    }
}
