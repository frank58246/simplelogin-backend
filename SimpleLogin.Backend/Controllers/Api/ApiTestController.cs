﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using SimpleLogin.Backend.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SimpleLogin.Backend.Controllers.Api
{
    [ApiController]
    [Route("[controller]")]
    public class ApiTestController : ControllerBase
    {

        [HttpGet]
        public async Task<string> Echo(string input)
        {
            return input;
        }

       
    }
}
