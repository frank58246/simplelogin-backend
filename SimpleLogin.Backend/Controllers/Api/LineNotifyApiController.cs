﻿using Microsoft.AspNetCore.Mvc;
using SimpleLogin.Backend.Extensions;
using SimpleLogin.Backend.Models.Response;
using SimpleLogin.Backend.Repository;
using SimpleLogin.Backend.Repository.Interfaces;
using SimpleLogin.Backend.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace SimpleLogin.Backend.Controllers.Api
{
    [ApiController]
    [Route("[controller]/[Action]")]
    public class LineNotifyApiController : ControllerBase
    {
        private readonly ILineNotifyTokenRepository _lineNotifyTokenRepository;

        private readonly HttpClient _httpClient;

        private readonly ILineNotifyTokenService _lineNotifyService;

        public LineNotifyApiController(ILineNotifyTokenRepository lineNotifyTokenRepository,
            IHttpClientFactory httpClientFactory, ILineNotifyTokenService lineNotifyService)
        {
            _lineNotifyTokenRepository = lineNotifyTokenRepository;
            _httpClient = httpClientFactory.CreateClient();
            _lineNotifyService = lineNotifyService;
        }

        [HttpPost]
        public async Task<string> SendMessageToAll(string message)
        {
            var url = "https://notify-api.line.me/api/notify";
            var tokens = this._lineNotifyTokenRepository.GetAll();
            var result = new StringBuilder();
            foreach (var token in tokens)
            {
                var payLoad = new Dictionary<string, string>
                {
                    { "message", message}
                };

                var response = await this._httpClient.PostFormAsync<BaseLineResponse>(url, payLoad, token);
                result.AppendLine(response.Message);
            }

            return result.ToString();
        }

        [HttpPost]
        public async Task<string> RevokeAllToken()
        {
            return await this._lineNotifyService.RevokeAllAsync();
        }
    }
}
