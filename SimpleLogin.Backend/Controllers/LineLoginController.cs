﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using SimpleLogin.Backend.Extensions;
using SimpleLogin.Backend.Models;
using SimpleLogin.Backend.Models.Request;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace SimpleLogin.Backend.Controllers
{
    public class LineLoginController : Controller
    {
        private readonly HttpClient _httpClient;

        private const string SESSION_NAME = "lineProfile";
        private const string NOTIFY_SESSION_NAME = "linenotify";

        private const string CLIENT_ID = "1657007023";
        private const string CLIENT_SECRET = "0738e9104c35a61e3962639b5379d25e";
        private const string STATE = "6666";
        private const string REDIRECT_URL = "https://simplelogin64285-backend.herokuapp.com/linelogin/callback";

        public LineLoginController(IHttpClientFactory httpClientFactory)
        {
            _httpClient = httpClientFactory.CreateClient();
        }

        public IActionResult Index()
        {
            if (HasProfileInSession())
            {
                return Redirect("/linelogin/Callback");
            }


            // show auth url
            var request = new AuthorizeRequest
            {
                AuthUrl = "https://access.line.me/oauth2/v2.1/authorize",
                ResponseType = "code",
                ClientId = "1657007023",
                RedirectUri = REDIRECT_URL,
                State = STATE,
                Scope = "openid profile"
            };

            return View("Index", request.ToRequestUrl());
        }


        public async Task<IActionResult> Callback(string state, string code)
        {
            if (state != STATE)
            {
                if (HasProfileInSession())
                {
                    var profileInSession = this.GetProfileFromSession();
                    ShowNotify();
                    return View("Callback", profileInSession);
                }

                return Redirect("/linelogin");
            }

            // get Token
            string url = "https://api.line.me/oauth2/v2.1/token";
            var payLoad = new Dictionary<string, string>()
            {
                { "grant_type", "authorization_code"},
                { "code", code},
                { "redirect_uri", REDIRECT_URL },
                { "client_id", CLIENT_ID},
                { "client_secret", CLIENT_SECRET}
            };

            var tokenReseponse = await _httpClient.PostFormAsync<LineLoginTokenResponse>(url, payLoad);
            if (!string.IsNullOrEmpty(tokenReseponse.Error))
            {
                return Redirect("/linelogin");
            }
         

            // get User Profile
            var profileUrl = "https://api.line.me/v2/profile";
            var profile = await _httpClient.GetAsync<LineProfileResponse>(profileUrl, tokenReseponse.AccessToken);
            SaveProfileInSession(profile);

            ShowNotify();



            return View("Callback", profile);
        }

        private void ShowNotify()
        {
            // get notify url       

            if (!HasTokenInSession())
            {
                var notifyRequest = new AuthorizeRequest
                {
                    AuthUrl = "https://notify-bot.line.me/oauth/authorize",
                    ResponseType = "code",
                    ClientId = "pZCvRTM2GzHGTBTgiWdQNf",
                    RedirectUri = "https://simplelogin64285-backend.herokuapp.com/linenotify/callback",
                    State = STATE,
                    Scope = "notify",
                    ResponseMode = "form_post"
                };
                ViewData["notifyUrl"] = notifyRequest.ToRequestUrl();
            }
            else
            {
                var token = GetTokenInSession();
                ViewData["notifyToken"] = token;
            }
        }

        private bool HasProfileInSession()
        {
            var value = HttpContext.Session.GetString(SESSION_NAME);

            if (string.IsNullOrEmpty(value))
            {
                return false;
            }

            try
            {
                var profile = JsonConvert.DeserializeObject<LineProfileResponse>(value);
                return true;
            }
            catch (Exception)
            {

                return false;
            }
        }

        private void SaveProfileInSession(LineProfileResponse profile)
        {
            var jsonString = JsonConvert.SerializeObject(profile);
            HttpContext.Session.SetString(SESSION_NAME, jsonString);
        }

        private LineProfileResponse GetProfileFromSession()
        {
            var value = HttpContext.Session.GetString(SESSION_NAME);
            return JsonConvert.DeserializeObject<LineProfileResponse>(value);          
        }

        private bool HasTokenInSession()
        {
            var value = HttpContext.Session.GetString(NOTIFY_SESSION_NAME);

            return !string.IsNullOrEmpty(value);
        }

        private string GetTokenInSession()
        {
            return HttpContext.Session.GetString(NOTIFY_SESSION_NAME);
        }

    }
}
