﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using SimpleLogin.Backend.Extensions;
using SimpleLogin.Backend.Models;
using SimpleLogin.Backend.Models.Request;
using SimpleLogin.Backend.Repository;
using SimpleLogin.Backend.Repository.Interfaces;
using SimpleLogin.Backend.Services.Interfaces;

namespace SimpleLogin.Backend.Controllers
{
    public class LineNotifyController : Controller
    {
        private readonly HttpClient _httpClient;
        private readonly ILineNotifyTokenService _lineNotifyTokenService;

        private const string SESSION_NAME = "linenotify";

        private const string CLIENT_ID = "pZCvRTM2GzHGTBTgiWdQNf";
        private const string CLIENT_SECRET = "JCxj2G4CVeyH24cD3FvmxcR81PWldvLco1yIJ5izCls";
        private const string STATE = "6666";
        private const string REDIRECT_URL = "https://simplelogin64285-backend.herokuapp.com/linenotify/callback";

        public LineNotifyController(IHttpClientFactory httpClientFactory, ILineNotifyTokenService lineNotifyService)
        {
            _httpClient = httpClientFactory.CreateClient();
            _lineNotifyTokenService = lineNotifyService;
        }

        public async Task<IActionResult> Index()
        {

            if (HasTokenInSession())
            {
                // call api to check token is validate
                var tokenInSession = GetTokenInSession();
                var isValidate = await this._lineNotifyTokenService.IsValidateAsync(tokenInSession);
                if (isValidate)
                {
                    return Redirect("/linelogin/callback");
                }
                else
                {
                    this.ClearTokenInSession();
                    this._lineNotifyTokenService.RemoveInMemory(tokenInSession);
                }
            }

            // show auth url
            var request = new AuthorizeRequest
            {
                AuthUrl = "https://notify-bot.line.me/oauth/authorize",
                ResponseType = "code",
                ClientId = CLIENT_ID,
                RedirectUri = REDIRECT_URL,
                State = STATE,
                Scope = "notify",
                ResponseMode = "form_post"
            };

            return View("Index", request.ToRequestUrl());
        }

        public async Task<IActionResult> Callback(string state, string code)
        {
            if (state == STATE)
            {
                // get Token
                string url = "https://notify-bot.line.me/oauth/token";
                var payLoad = new Dictionary<string, string>()
                    {
                        { "grant_type", "authorization_code"},
                        { "code", code},
                        { "redirect_uri", REDIRECT_URL },
                        { "client_id", CLIENT_ID},
                        { "client_secret", CLIENT_SECRET}
                    };

                var tokenResponse = await _httpClient.PostFormAsync<LineNotifyTokenResponse>(url, payLoad);

                // check status
                if (tokenResponse.Status != (int)HttpStatusCode.OK)
                {
                    return Redirect("/linelogin");
                }

                // save token
                this._lineNotifyTokenService.AddInMemory(tokenResponse.AccessToken);
                this.SaveTokenInSession(tokenResponse.AccessToken);

                return Redirect("/linelogin/callback");
                //return View("Callback", tokenResponse.Message + $" your token is {tokenResponse.AccessToken}");

            }

            if (HasTokenInSession())
            {
                // call api to check token is validate
                var tokenInSession = GetTokenInSession();
                var isValidate = await this._lineNotifyTokenService.IsValidateAsync(tokenInSession);
                if (isValidate)
                {
                    return Redirect("/linelogin/callback");
                    return View("Callback", $"your token is {tokenInSession}");
                }

            }

            return Redirect("/linelogin");

        }

        public async Task<IActionResult> Revoke()
        {
            if (!HasTokenInSession())
            {
                return Redirect("/lineNotify");
            }

            // clear session, memory, then revoke
            var token = this.GetTokenInSession();

            this.ClearTokenInSession();

            this._lineNotifyTokenService.RemoveInMemory(token);

            await this._lineNotifyTokenService.RevokeAsync(token);

            return Redirect("/lineNotify");
        }



        private bool HasTokenInSession()
        {
            var value = HttpContext.Session.GetString(SESSION_NAME);

            return !string.IsNullOrEmpty(value);
        }

        private string GetTokenInSession()
        {
            return HttpContext.Session.GetString(SESSION_NAME);
        }

        private void SaveTokenInSession(string token)
        {
            HttpContext.Session.SetString(SESSION_NAME, token);
        }


        private void ClearTokenInSession()
        {
            HttpContext.Session.SetString(SESSION_NAME, string.Empty);
        }
    }
}
