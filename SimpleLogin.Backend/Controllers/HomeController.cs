﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;

namespace SimpleLogin.Backend.Controllers
{
    public class HomeController : Controller
    {
        public IActionResult Index()
        {
            var a = HttpContext.Session.GetString("A");
            var b = HttpContext.Session.GetString("B");
            return View("Index");
        }

        public IActionResult A()
        {
            HttpContext.Session.SetString("A","A");
            return Redirect("/Home");
        }

        public IActionResult B()
        {
            HttpContext.Session.SetString("B","B");
            return Redirect("/Home");
        }

    }
}
