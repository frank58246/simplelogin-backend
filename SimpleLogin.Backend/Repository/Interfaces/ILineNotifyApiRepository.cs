﻿using SimpleLogin.Backend.Models.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SimpleLogin.Backend.Repository.Interfaces
{
    public interface ILineNotifyApiRepository
    {
        public Task<string> RevokeAsync(string token);

        public Task<BaseLineResponse> GetTokenStatusAsync(string token);
    }
}
