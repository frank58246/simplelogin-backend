﻿using SimpleLogin.Backend.Extensions;
using SimpleLogin.Backend.Models.Response;
using SimpleLogin.Backend.Repository.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace SimpleLogin.Backend.Repository
{
    public class LineNotifyApiRepository : ILineNotifyApiRepository
    {
        private readonly HttpClient _httpClient;

        public LineNotifyApiRepository(IHttpClientFactory httpClientFactory)
        {
            _httpClient = httpClientFactory.CreateClient() ;
        }

        public async Task<BaseLineResponse> GetTokenStatusAsync(string token)
        {
            var url = "https://notify-api.line.me/api/status";           
            return await this._httpClient.GetAsync<BaseLineResponse>(url, token);
        }

        public async Task<string> RevokeAsync(string token)
        {
            var url = "https://notify-api.line.me/api/revoke";

            var payload = new Dictionary<string, string>();

            var response  = await this._httpClient.PostFormAsync<BaseLineResponse>(url,payload,token);

            return response.Message;
        }

        
    }
}
