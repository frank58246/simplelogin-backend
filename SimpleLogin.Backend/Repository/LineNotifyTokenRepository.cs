﻿using SimpleLogin.Backend.Repository.Interfaces;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SimpleLogin.Backend.Repository
{

    /// <summary>
    /// Use Singleton DI to serve as Database
    /// </summary>
    public class LineNotifyTokenRepository: ILineNotifyTokenRepository
    {
        private HashSet<string> _allToken;

        public LineNotifyTokenRepository()
        {
            this._allToken = new HashSet<string>();
        }

        public IEnumerable<string> GetAll()
        {
            return _allToken.ToList();
        }

        public bool Remove(string token)
        {
            return this._allToken.Remove(token);

        }

        public bool Save(string token)
        {
           return this._allToken.Add(token);
        }
    }
}
