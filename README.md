# SimpleLogin-Backend

Line OAuth Practice

## LineLogin
- 登入前: /LineLogin 
- 登入後: /LineLogin/Callback
- 使用OAtuh取得Profile後，存入session
- 如果使用者session有存值，會直接被Redirect到登入後

## LineNotify
- 連接前: /LineNotify
- 連接後: /LineNotify/Callback

- 使用OAuth取得access token後，會存在memory和session
- 如果session有值且合法，會直接被redirect到連接後的頁面
- 如果直接訪問連接後頁面，session沒值，且不是OAuth的Callback呼叫，會Redirect到連接前的頁面
- 可以在Swagger 撤銷token或是發送訊息
- 發送訊息的對象，僅限存在於memory的access token